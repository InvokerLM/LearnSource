﻿using System;

namespace Boying.Mvc
{
    public class HttpContextWorkContext : IWorkContextStateProvider
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public HttpContextWorkContext(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public Func<WorkContext, T> Get<T>(string name)
        {
            if (name == "HttpContext")
            {
                //调用Current,返回HttpContextWrapper
                var result = (T)(object)_httpContextAccessor.Current();
                return ctx => result;
            }
            return null;
        }
    }
}