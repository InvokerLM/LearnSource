using System;
using System.Web;
using Autofac;

namespace Boying.Mvc
{
    public class HttpContextAccessor : IHttpContextAccessor
    {
        private readonly ILifetimeScope _lifetimeScope;
        private HttpContextBase _httpContext;
        private IWorkContextAccessor _wca;

        public HttpContextAccessor(ILifetimeScope lifetimeScope)
        {
            _lifetimeScope = lifetimeScope;
        }

        public HttpContextBase Current()
        {
            //获取当前请求上下文
            var httpContext = GetStaticProperty();
            //如果不是后台请求，则发挥HttpContextWrapper
            if (!IsBackgroundHttpContext(httpContext))
                return new HttpContextWrapper(httpContext);
            //如果_httpContext不为空则返回，可以通过Set方法设置_httpContext
            if (_httpContext != null)
                return _httpContext;
            //如果在生命周期中注册了WorkContextAccessor则获取
            if (_wca == null && _lifetimeScope.IsRegistered<IWorkContextAccessor>())
                _wca = _lifetimeScope.Resolve<IWorkContextAccessor>();
            //获取逻辑线程唯一的workContext对象，如果没有则为空
            var workContext = _wca != null ? _wca.GetLogicalContext() : null;
            //这里会调用HttpContextWorkContext中的Get方法，最终又调用了当前Current方法
            return workContext != null ? workContext.HttpContext : null;
        }

        public void Set(HttpContextBase httpContext)
        {
            _httpContext = httpContext;
        }

        internal static bool IsBackgroundHttpContext(HttpContext httpContext)
        {
            //是否是后台HttpContext
            return httpContext == null || httpContext.Items.Contains(MvcModule.IsBackgroundHttpContextKey);
        }

        private static HttpContext GetStaticProperty()
        {
            var httpContext = HttpContext.Current;
            if (httpContext == null)
            {
                return null;
            }

            try
            {
                // The "Request" property throws at application startup on IIS integrated pipeline mode.
                if (httpContext.Request == null)
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
            return httpContext;
        }
    }
}