﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Autofac;

namespace Boying.Environment
{
    internal class WorkContextImplementation : WorkContext
    {
        private readonly IComponentContext _componentContext;
        //可多线程同时访问
        private readonly ConcurrentDictionary<string, Func<object>> _stateResolvers = new ConcurrentDictionary<string, Func<object>>();
        private readonly IEnumerable<Lazy<IWorkContextStateProvider>> _workContextStateProviders;

        public WorkContextImplementation(IComponentContext componentContext)
        {
            _componentContext = componentContext;
            _workContextStateProviders = componentContext.Resolve<IEnumerable<Lazy<IWorkContextStateProvider>>>();
        }

        public override T Resolve<T>()
        {
            return _componentContext.Resolve<T>();
        }

        public override object Resolve(Type serviceType)
        {
            return _componentContext.Resolve(serviceType);
        }

        public override bool TryResolve<T>(out T service)
        {
            return _componentContext.TryResolve(out service);
        }

        public override bool TryResolve(Type serviceType, out object service)
        {
            return _componentContext.TryResolve(serviceType, out service);
        }

        public override T GetState<T>(string name)
        {
            //获取或者添加针对某个key的Func操作
            var resolver = _stateResolvers.GetOrAdd(name, FindResolverForState<T>);
            //执行传出的表达式，即执行IworkContextStateProvider的Get方法
            return (T)resolver();
        }

        private Func<object> FindResolverForState<T>(string name)
        {
            //所有继承了WorkContextStateProvider的实现类，获取一个Func<WorkContext,T>的集合中T相符合的第一个不为空Func
            var resolver = _workContextStateProviders.Select(wcsp => wcsp.Value.Get<T>(name)).FirstOrDefault(value => !Equals(value, default(T)));
            //如果没有，则返回一个空的T对象
            if (resolver == null)
            {
                return () => default(T);
            }
            //如果存在，则构造lambda表达式，用于执行该方法，将当前WorkContext传入
            return () => resolver(this);
        }

        public override void SetState<T>(string name, T value)
        {
            _stateResolvers[name] = () => value;
        }
    }
}