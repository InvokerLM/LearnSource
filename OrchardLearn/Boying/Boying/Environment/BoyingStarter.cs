﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;
using Autofac;
using Autofac.Configuration;
using Boying.Caching;
using Boying.Data;
using Boying.Environment.AutofacUtil;
using Boying.Environment.Configuration;
using Boying.Environment.Descriptor;
using Boying.Environment.Extensions;
using Boying.Environment.Extensions.Compilers;
using Boying.Environment.Extensions.Folders;
using Boying.Environment.Extensions.Loaders;
using Boying.Environment.ShellBuilders;
using Boying.Environment.State;
using Boying.Events;
using Boying.Exceptions;
using Boying.FileSystems.AppData;
using Boying.FileSystems.Dependencies;
using Boying.FileSystems.LockFile;
using Boying.FileSystems.VirtualPath;
using Boying.FileSystems.WebSite;
using Boying.Logging;
using Boying.Mvc;
using Boying.Mvc.DataAnnotations;
using Boying.Mvc.Filters;
using Boying.Mvc.ViewEngines.Razor;
using Boying.Mvc.ViewEngines.ThemeAwareness;
using Boying.Services;
using Boying.WebApi;
using Boying.WebApi.Filters;

namespace Boying.Environment
{
    public static class BoyingStarter
    {
        //所有服务注册到Autofac容器中
        public static IContainer CreateHostContainer(Action<ContainerBuilder> registrations)
        {
            ExtensionLocations extensionLocations = new ExtensionLocations();

            var builder = new ContainerBuilder();
            // Application paths and parameters
            //注入一个手动实例化的ExtensionLocations实例，RegisterInstance是单例模式注入，程序内共享同一个实例
            builder.RegisterInstance(extensionLocations);
            //替换Collection顺序，先进先出
            builder.RegisterModule(new CollectionOrderModule());
            //注册Logger,然后替换ILogger属性为CastleLogger实例
            builder.RegisterModule(new LoggingModule());
            //注册IEventBus事件总线
            builder.RegisterModule(new EventsModule());
            builder.RegisterModule(new CacheModule());

            // a single default host implementation is needed for bootstrapping a web app domain
            builder.RegisterType<DefaultBoyingEventBus>().As<IEventBus>().SingleInstance();
            //CacheHolder缓存存储区，会在DefaultCacheManager构造的时候调用
            builder.RegisterType<DefaultCacheHolder>().As<ICacheHolder>().SingleInstance();
            //ChacheContextAccessor被holder引用,主要用于设置基于线程唯一的IAcquireContext,
            builder.RegisterType<DefaultCacheContextAccessor>().As<ICacheContextAccessor>().SingleInstance();
            //并行ParallelCacheContext,ChacheContextAccess作为属性
            builder.RegisterType<DefaultParallelCacheContext>().As<IParallelCacheContext>().SingleInstance();
            //异步验证token
            builder.RegisterType<DefaultAsyncTokenProvider>().As<IAsyncTokenProvider>().SingleInstance();
            //初始化需要Clock,HttpContextAccessor
            builder.RegisterType<DefaultHostEnvironment>().As<IHostEnvironment>().SingleInstance();
            //主机重启操作，依赖AppDataFolder
            builder.RegisterType<DefaultHostLocalRestart>().As<IHostLocalRestart>().Named<IEventHandler>(typeof(IShellSettingsManagerEventHandler).Name).SingleInstance();
            builder.RegisterType<DefaultBuildManager>().As<IBuildManager>().SingleInstance();
            builder.RegisterType<DynamicModuleVirtualPathProvider>().As<ICustomVirtualPathProvider>().SingleInstance();
            //单例注册AppDataFolderRoot，用于提供RootPath相对路径与RootFolder绝对路径
            builder.RegisterType<AppDataFolderRoot>().As<IAppDataFolderRoot>().SingleInstance();
            builder.RegisterType<DefaultExtensionCompiler>().As<IExtensionCompiler>().SingleInstance();
            builder.RegisterType<DefaultRazorCompilationEvents>().As<IRazorCompilationEvents>().SingleInstance();
            builder.RegisterType<DefaultProjectFileParser>().As<IProjectFileParser>().SingleInstance();
            builder.RegisterType<DefaultAssemblyLoader>().As<IAssemblyLoader>().SingleInstance();
            builder.RegisterType<AppDomainAssemblyNameResolver>().As<IAssemblyNameResolver>().SingleInstance();
            builder.RegisterType<GacAssemblyNameResolver>().As<IAssemblyNameResolver>().SingleInstance();
            builder.RegisterType<BoyingFrameworkAssemblyNameResolver>().As<IAssemblyNameResolver>().SingleInstance();
            //每次实例化一个新的HttpContextAccessor,里面进行了生命周期域的管理
            builder.RegisterType<HttpContextAccessor>().As<IHttpContextAccessor>().InstancePerDependency();
            builder.RegisterType<ViewsBackgroundCompilation>().As<IViewsBackgroundCompilation>().SingleInstance();
            builder.RegisterType<DefaultExceptionPolicy>().As<IExceptionPolicy>().SingleInstance();
            builder.RegisterType<DefaultCriticalErrorProvider>().As<ICriticalErrorProvider>().SingleInstance();
            //builder.RegisterType<RazorTemplateCache>().As<IRazorTemplateProvider>().SingleInstance();

            RegisterVolatileProvider<WebSiteFolder, IWebSiteFolder>(builder);
            //AppDataFoler单例，依赖于AppDataFolderRoot和VirtualPathMonitor
            RegisterVolatileProvider<AppDataFolder, IAppDataFolder>(builder);
            RegisterVolatileProvider<DefaultLockFileManager, ILockFileManager>(builder);
            //Clock单例模式，调用生成具体的IVoilateToken实例
            RegisterVolatileProvider<Clock, IClock>(builder);
            RegisterVolatileProvider<DefaultDependenciesFolder, IDependenciesFolder>(builder);
            RegisterVolatileProvider<DefaultExtensionDependenciesManager, IExtensionDependenciesManager>(builder);
            RegisterVolatileProvider<DefaultAssemblyProbingFolder, IAssemblyProbingFolder>(builder);
            //DefaultVirtualPathMonitor单例注册
            RegisterVolatileProvider<DefaultVirtualPathMonitor, IVirtualPathMonitor>(builder);
            RegisterVolatileProvider<DefaultVirtualPathProvider, IVirtualPathProvider>(builder);

            builder.RegisterType<DefaultBoyingHost>().As<IBoyingHost>().As<IEventHandler>()
                .Named<IEventHandler>(typeof(IShellSettingsManagerEventHandler).Name)
                .Named<IEventHandler>(typeof(IShellDescriptorManagerEventHandler).Name)
                .SingleInstance();
            {
                builder.RegisterType<ShellSettingsManager>().As<IShellSettingsManager>().SingleInstance();

                builder.RegisterType<ShellContextFactory>().As<IShellContextFactory>().SingleInstance();
                {
                    builder.RegisterType<ShellDescriptorCache>().As<IShellDescriptorCache>().SingleInstance();

                    builder.RegisterType<CompositionStrategy>().As<ICompositionStrategy>().SingleInstance();
                    {
                        builder.RegisterType<ShellContainerRegistrations>().As<IShellContainerRegistrations>().SingleInstance();
                        builder.RegisterType<ExtensionLoaderCoordinator>().As<IExtensionLoaderCoordinator>().SingleInstance();
                        builder.RegisterType<ExtensionMonitoringCoordinator>().As<IExtensionMonitoringCoordinator>().SingleInstance();
                        builder.RegisterType<ExtensionManager>().As<IExtensionManager>().SingleInstance();
                        {
                            builder.RegisterType<ExtensionHarvester>().As<IExtensionHarvester>().SingleInstance();
                            builder.RegisterType<ModuleFolders>().As<IExtensionFolders>().SingleInstance()
                                .WithParameter(new NamedParameter("paths", extensionLocations.ModuleLocations));
                            builder.RegisterType<CoreModuleFolders>().As<IExtensionFolders>().SingleInstance()
                                .WithParameter(new NamedParameter("paths", extensionLocations.CoreLocations));
                            builder.RegisterType<ThemeFolders>().As<IExtensionFolders>().SingleInstance()
                                .WithParameter(new NamedParameter("paths", extensionLocations.ThemeLocations));

                            builder.RegisterType<CoreExtensionLoader>().As<IExtensionLoader>().SingleInstance();
                            builder.RegisterType<ReferencedExtensionLoader>().As<IExtensionLoader>().SingleInstance();
                            builder.RegisterType<PrecompiledExtensionLoader>().As<IExtensionLoader>().SingleInstance();
                            builder.RegisterType<DynamicExtensionLoader>().As<IExtensionLoader>().SingleInstance();
                            builder.RegisterType<RawThemeExtensionLoader>().As<IExtensionLoader>().SingleInstance();
                        }
                    }

                    builder.RegisterType<ShellContainerFactory>().As<IShellContainerFactory>().SingleInstance();
                }

                builder.RegisterType<DefaultProcessingEngine>().As<IProcessingEngine>().SingleInstance();
            }

            builder.RegisterType<RunningShellTable>().As<IRunningShellTable>().SingleInstance();
            builder.RegisterType<DefaultBoyingShell>().As<IBoyingShell>().InstancePerMatchingLifetimeScope("shell");
            builder.RegisterType<SessionConfigurationCache>().As<ISessionConfigurationCache>().InstancePerMatchingLifetimeScope("shell");
            //注册MVC中的Routes\Binders\Engines
            registrations(builder);
            //判断是否存在Autofac Section用于配置Autofac,如果存在则用于服务注册
            var autofacSection = ConfigurationManager.GetSection(ConfigurationSettingsReaderConstants.DefaultSectionName);
            if (autofacSection != null)
                builder.RegisterModule(new ConfigurationSettingsReader());
            //检查是否存在Host.Config，如果存在则应用配置注册服务
            var optionalHostConfig = HostingEnvironment.MapPath("~/Config/Host.config");
            if (File.Exists(optionalHostConfig))
                builder.RegisterModule(new ConfigurationSettingsReader(ConfigurationSettingsReaderConstants.DefaultSectionName, optionalHostConfig));
            //检查是否存在HostComponents.config，如果存在则应用配置注册服务
            var optionalComponentsConfig = HostingEnvironment.MapPath("~/Config/HostComponents.config");
            if (File.Exists(optionalComponentsConfig))
                builder.RegisterModule(new HostComponentsConfigModule(optionalComponentsConfig));

            var container = builder.Build();

            //
            // 注册虚拟路径处理
            //
            if (HostingEnvironment.IsHosted)
            {
                foreach (var vpp in container.Resolve<IEnumerable<ICustomVirtualPathProvider>>())
                {
                    HostingEnvironment.RegisterVirtualPathProvider(vpp.Instance);
                }
            }
            //重写ControllerFactory通过容器获取服务
            ControllerBuilder.Current.SetControllerFactory(new BoyingControllerFactory());
            //提供filter获取
            FilterProviders.Providers.Add(new BoyingFilterProvider());

            GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpControllerSelector), new DefaultBoyingWebApiHttpControllerSelector(GlobalConfiguration.Configuration));
            GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpControllerActivator), new DefaultBoyingWebApiHttpControllerActivator(GlobalConfiguration.Configuration));
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            GlobalConfiguration.Configuration.Filters.Add(new BoyingApiActionFilterDispatcher());
            GlobalConfiguration.Configuration.Filters.Add(new BoyingApiExceptionFilterDispatcher());
            GlobalConfiguration.Configuration.Filters.Add(new BoyingApiAuthorizationFilterDispatcher());

            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new ThemeAwareViewEngineShim());

            var hostContainer = new DefaultBoyingHostContainer(container);
            //MvcServiceLocator.SetCurrent(hostContainer);
            BoyingHostContainerRegistry.RegisterHostContainer(hostContainer);

            // Register localized data annotations
            ModelValidatorProviders.Providers.Clear();
            ModelValidatorProviders.Providers.Add(new LocalizedModelValidatorProvider());

            return container;
        }

        //注册全局唯一单例
        private static void RegisterVolatileProvider<TRegister, TService>(ContainerBuilder builder) where TService : IVolatileProvider
        {
            builder.RegisterType<TRegister>()
                .As<TService>()
                .As<IVolatileProvider>()
                .SingleInstance();
        }

        public static IBoyingHost CreateHost(Action<ContainerBuilder> registrations)
        {
            //1、执行CreatHostContainer中的方法
            //2、将registrations委托给Global.asax中的MvcSingletons执行
            //MvcSingletons中的代码，将路由、模型绑定器、视图引擎进行单例注入
            //builder.Register(ctx => RouteTable.Routes).SingleInstance();
            //builder.Register(ctx => ModelBinders.Binders).SingleInstance();
            //builder.Register(ctx => ViewEngines.Engines).SingleInstance();
            var container = CreateHostContainer(registrations);//使用autofac完成服务注入后，返回服务容器
            //从容器中获取到BoyingHost主机实例
            return container.Resolve<IBoyingHost>();
        }
    }
}