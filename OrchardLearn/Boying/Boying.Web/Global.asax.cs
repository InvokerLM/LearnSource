﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Autofac;
using Boying.Environment;
using Boying.WarmupStarter;

namespace Boying.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode,
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : HttpApplication
    {
        private static Starter<IBoyingHost> _starter;

        public MvcApplication()
        {
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
        }

        protected void Application_Start()
        {
            RegisterRoutes(RouteTable.Routes);
            _starter = new Starter<IBoyingHost>(HostInitialization, HostBeginRequest, HostEndRequest);
            //Starter启动器委托Global.asax代为实现初始化操作，以及请求前与请求后的处理
            _starter.OnApplicationStart(this);
        }

        protected void Application_BeginRequest()
        {
            _starter.OnBeginRequest(this);
        }

        protected void Application_EndRequest()
        {
            _starter.OnEndRequest(this);
        }

        private static void HostBeginRequest(HttpApplication application, IBoyingHost host)
        {
            application.Context.Items["originalHttpContext"] = application.Context;
            host.BeginRequest();
        }

        private static void HostEndRequest(HttpApplication application, IBoyingHost host)
        {
            host.EndRequest();
        }

        private static IBoyingHost HostInitialization(HttpApplication application)
        {
            //调用指定的启动器来创建主机
            var host = BoyingStarter.CreateHost(MvcSingletons);

            host.Initialize();

            // initialize shells to speed up the first dynamic query
            host.BeginRequest();
            host.EndRequest();

            return host;
        }

        private static void MvcSingletons(ContainerBuilder builder)
        {
            //注册为单例
            builder.Register(ctx => RouteTable.Routes).SingleInstance();
            builder.Register(ctx => ModelBinders.Binders).SingleInstance();
            builder.Register(ctx => ViewEngines.Engines).SingleInstance();
        }
    }
}